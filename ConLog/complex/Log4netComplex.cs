﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConLog
{
    public class Log4netComplex
    {
        static ILog _infoLogger = LogManager.GetLogger("loginfo");
        static ILog _warnLogger = LogManager.GetLogger("logwarn");
        static ILog _errorLogger = LogManager.GetLogger("logerror");
        static ILog _fatalLogger = LogManager.GetLogger("logfatal");
        static Log4netComplex()
        {
            var logCfg = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "complex/log4net.complex.config");
            XmlConfigurator.ConfigureAndWatch(logCfg);

            log4net.MDC.Set("userid", "1");
            log4net.MDC.Set("ip", "your ip");
        }

        public static void Info(string msg)
        {
            _infoLogger.Info(msg);
        }

        public static void Warn(string msg)
        {
            _warnLogger.Warn(msg);
        }

        public static void Error(string msg, Exception ex = null)
        {
            _errorLogger.Error(msg, ex);
        }

        public static void Fatal(string msg, Exception ex = null)
        {
            _fatalLogger.Fatal(msg, ex);
        }
    }
}
