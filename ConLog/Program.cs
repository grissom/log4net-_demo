﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConLog
{
    class Program
    {
        static void Main(string[] args)
        {
            //-- 简单用法

            //Log4netSimple.Info("消息");
            //Log4netSimple.Warn("警告");
            //Log4netSimple.Error("异常");
            //Log4netSimple.Fatal("错误");


            //-- 强悍用法

            Log4netComplex.Info("消息");
            Log4netComplex.Warn("警告");
            try
            {
                Convert.ToDateTime("abc");
            }
            catch (Exception ex)
            {

                Log4netComplex.Error("异常", ex);
                Log4netComplex.Fatal("错误", ex);
            }

            Console.ReadKey();
        }
    }
}
