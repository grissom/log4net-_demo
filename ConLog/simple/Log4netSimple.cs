﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConLog
{
    public class Log4netSimple
    {
        static ILog _logger = LogManager.GetLogger("");
        static Log4netSimple()
        {
            var logCfg = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "simple/log4net.simple.config");
            XmlConfigurator.ConfigureAndWatch(logCfg);
        }

        public static void Info(string msg)
        {
            _logger.Info(msg);
        }

        public static void Warn(string msg)
        {
            _logger.Warn(msg);
        }

        public static void Error(string msg, Exception ex = null)
        {
            _logger.Error(msg, ex);
        }

        public static void Fatal(string msg, Exception ex = null)
        {
            _logger.Fatal(msg, ex);
        }
    }
}
